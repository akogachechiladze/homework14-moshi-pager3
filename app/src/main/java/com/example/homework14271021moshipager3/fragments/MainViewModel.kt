package com.example.homework14271021moshipager3.fragments


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.homework14271021moshipager3.UPagingSource

class MainViewModel: ViewModel() {

    fun loadUsers() {
        Pager(config = PagingConfig(pageSize = 1), pagingSourceFactory = {UPagingSource()}).liveData.cachedIn(viewModelScope)
    }


}

