package com.example.homework14271021moshipager3.fragments


import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework14271021moshipager3.adapter.RecyclerAdapter
import com.example.homework14271021moshipager3.databinding.FragmentMainBinding


class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var adapter: RecyclerAdapter

    override fun start() {
        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        adapter = RecyclerAdapter()
        binding.recycler.adapter = adapter
        viewModel.loadUsers().observe(viewLifecycleOwner, {
            adapter.submitData(lifecycle, it)
        })
    }



}