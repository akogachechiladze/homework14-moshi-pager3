package com.example.homework14271021moshipager3.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.homework14271021moshipager3.R

fun ImageView.setImage(imageUrl: String? = null) {
    if (!imageUrl.isNullOrEmpty())
        Glide.with(context).load(imageUrl).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(this)
    else
        setImageResource(R.mipmap.ic_launcher)
}