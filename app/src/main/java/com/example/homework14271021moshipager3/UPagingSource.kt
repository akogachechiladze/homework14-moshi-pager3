package com.example.homework14271021moshipager3

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.homework14271021moshipager3.model.UsersItems
import com.example.homework14271021moshipager3.network.NetworkClient

class UPagingSource: PagingSource<Int, UsersItems.Data>() {
    override fun getRefreshKey(state: PagingState<Int, UsersItems.Data>): Int? {
        
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UsersItems.Data> {
        val page: Int = params.key ?:1


        return try {
            val response = NetworkClient.apiClient.getUsers(page)
            val body = response.body()

            if (response.isSuccessful && body != null) {
                var prevPage: Int? = null
                var nextPage: Int? = null
                if (body.totalPages > body.page) {
                    nextPage = page+1
                }

                if (page !=1) {
                    prevPage = page - 1
                }
                LoadResult.Page(
                    body.data!!,
                    prevPage,
                    nextPage
                )
            }else {
                LoadResult.Error(Throwable())
            }
        }catch (e:Exception) {
            LoadResult.Error(e)
        }
    }


}