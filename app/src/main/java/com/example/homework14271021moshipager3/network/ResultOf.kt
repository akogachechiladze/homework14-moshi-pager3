//package com.example.homework14271021moshipager3.network
//
//
//
//sealed class ResultOf<out T> {
//    data class Loading(val isRefreshing: Boolean): ResultOf<Nothing>()
//    data class Success<out R>(val value: R): ResultOf<R>()
//    data class Failure(val message: String?, val throwable: Throwable?): ResultOf<Nothing>()
//
//}





































//sealed class Result<out T> {
//
//    data class Success<out T>(val data: T?) :Result<T>()
//
//    data class Error<out T>(val code: Int? = null, val error: Exception? = null): Result<T>()
//
//    object NetworkError: Result<Nothing>()
//
//
//
//    //functions
//    fun isSuccess() = this is Success<*>
//    fun isError() = this is Error<*>
//    fun data() = if (isSuccess()) (this as Success<T>).data else null
//
//}
//
//
//fun successResult() = Result.Success(null)
//fun <T> successResult(data: T?) = Result.Success(data)
//fun errorResult() = Result.Error<Nothing>(null)
