package com.example.homework14271021moshipager3.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.homework14271021moshipager3.databinding.ItemsLayoutBinding
import com.example.homework14271021moshipager3.extensions.setImage
import com.example.homework14271021moshipager3.model.UsersItems

//pagingis adapter
class RecyclerAdapter() : PagingDataAdapter<UsersItems.Data, RecyclerAdapter.ViewHolder>(

) {

    inner class ViewHolder( val binding: ItemsLayoutBinding ): RecyclerView.ViewHolder(binding.root){
        fun onBind(model: UsersItems.Data) {
            binding.IV1.setImage(model.avatar)
            binding.TV1.text = model.firstName.plus(model.lastName)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position)!!)
    }

    class callBack: DiffUtil.ItemCallback<UsersItems.Data>() {
        override fun areItemsTheSame(oldItem: UsersItems.Data, newItem: UsersItems.Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UsersItems.Data, newItem: UsersItems.Data): Boolean {
            return oldItem == newItem
        }

    }

}